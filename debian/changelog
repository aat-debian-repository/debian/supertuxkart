supertuxkart (1.4+dfsg-2) unstable; urgency=medium

  * Team upload.

  [ Michael R. Crusoe ]
  * As per Debian Policy 7.8 and the GPL, document which version of libsimde-dev
    was used to build the binary package.

  [ Sam Q ]
  * Drop symlinks to fonts in large font packages and drop dependencies on
    them. This will use the distributed font copies, but allows one to save
    more than 400 MB of dependencies. (Closes: #975575)

  [ Reiner Herrmann ]
  * Replace obsolete build dependencies.
  * Bump Standards-Version to 4.6.2.

 -- Reiner Herrmann <reiner@reiner-h.de>  Tue, 28 Feb 2023 23:00:54 +0100

supertuxkart (1.4+dfsg-1) unstable; urgency=medium

  * Team upload.
  * New upstream release.
    - drop patches applied upstream
    - drop new embedded libraries that are available in Debian and build
      STK with the system libraries
    - drop auto-generated help files from source tarball
  * Bump Standards-Version to 4.6.1.
  * Remove Sam Hocevar from uploaders list as requested by MIA team.
    Thanks to Sam for maintaining STK in the past. (Closes: #1011587)
  * Prevent installation of files from the updated angelscript.
  * Update copyrights.
  * Drop repack count from d/watch.

 -- Reiner Herrmann <reiner@reiner-h.de>  Mon, 09 Jan 2023 12:20:10 +0100

supertuxkart (1.3+dfsg1-3) unstable; urgency=medium

  * Team upload.
  * Fix FTBFS on armhf.
    Thanks to Michael Hudson-Doyle for the patch (Closes: #1006141)

 -- Reiner Herrmann <reiner@reiner-h.de>  Tue, 22 Feb 2022 01:23:34 +0100

supertuxkart (1.3+dfsg1-2) unstable; urgency=medium

  * Team upload.
  * Import patch to fix FTBFS on mips. (Closes: #995839)

 -- Reiner Herrmann <reiner@reiner-h.de>  Thu, 07 Oct 2021 23:37:58 +0200

supertuxkart (1.3+dfsg1-1) unstable; urgency=medium

  * Team upload.
  * New upstream release:
    - update Files-Excluded for new release
    - drop asset-replacements which are now part of new release
    - drop asset replacements from d/source/include-binaries
    - drop embedded libraries dnsc and mojoal from upstream source
    - drop patches applied upstream and refresh others
  * Cherry-pick upstream patch that updates bundled cert store.
  * Drop libglew-dev from build dependencies.
  * Build-depend on libmbedtls-dev instead of nettle-dev for cryptography.
    - nettle support has been dropped upstream
  * Bump Standards-Version to 4.6.0.
  * Update d/copyright.
  * Drop d/clean.
  * /usr/share/pixmaps is no longer installed.
  * Remove dbgsym migration, as it has been released in stable.
  * Symlink two new font files.

 -- Reiner Herrmann <reiner@reiner-h.de>  Sun, 03 Oct 2021 17:52:46 +0200

supertuxkart (1.2+ds2-1) unstable; urgency=medium

  * Team upload.
  * Repack upstream tarball to drop non-free assets: (Closes: #990368)
    - the karts beastie and hexley have been removed
    - remove unused files with unknown license status:
      roof_test.png, stone-gloss.jpg, window.png
    - replace assets with unknown license status:
      img_0572.png, icon-sara.png, jump.ogg, plopp.ogg
  * d/copyright: Sync license and copyright information with upstream
    stk-assets repo. Thanks to deve and benau for license investigations and
    asset replacements.
  * d/rules: Copy replaced assets into data directory.
  * Cherry-pick upstream patches to keep network compatibility when official
    karts are missing.

 -- Reiner Herrmann <reiner@reiner-h.de>  Sun, 25 Jul 2021 12:48:11 +0200

supertuxkart (1.2+ds-2) unstable; urgency=medium

  * Team upload.
  * Cherry-pick patch to fix FTBFS with SDL 2.0.14. (Closes: #980606)
  * Bump Standards-Version to 4.5.1.
  * Drop unused lintian-override.

 -- Reiner Herrmann <reiner@reiner-h.de>  Sat, 30 Jan 2021 16:44:06 +0100

supertuxkart (1.2+ds-1) unstable; urgency=medium

  * Team upload.

  [ QwertyChouskie ]
  * New upstream release:
    - Drop remove_irrlicht_embedded_libs.patch
    - Remove remove_irrlicht_embedded_libs.patch from series
  * Add libsdl2-dev to build-deps. STK 1.2 ported its window creation and
    gamepad handling to SDL2.
  * Update supertuxkart.manpages with correct filename for 1.2.

  [ Reiner Herrmann ]
  * Refresh patches and drop fix-hurd-ftbfs.diff (no longer applicable).
  * Remove paths no longer available upstream from d/rules and d/copyright.
  * Update debhelper compat level to 13:
    - Mark wiiuse.h and libwiiuse.a as not-installed
  * Bump Standards-Version to 4.5.0.
  * Depend on newly used fonts and symlink to them.
  * d/copyright:
    - Document copyright of newly embedded library sheenbidi and new fonts
    - Drop android directory from source tarball via Files-Excluded
    - Update copyrights
  * Point d/watch to Github.
  * Drop unneeded build-dependencies: libbz2-dev, libfribidi-dev, libraqm-dev.
  * Drop from build-dependencies: libwayland-dev, libxkbcommon-dev,
    libegl1-mesa-dev, because window handling is now performed with SDL2.

 -- Reiner Herrmann <reiner@reiner-h.de>  Fri, 28 Aug 2020 18:52:58 +0200

supertuxkart (1.1+ds-1) unstable; urgency=medium

  * Team upload.
  * Update d/watch to version 4 and add repacksuffix.
  * Re-add embedded copy/fork of enet.
  * Enable IPv6 support.
  * Drop sources of files from missing-sources that are no longer shipped.

 -- Reiner Herrmann <reiner@reiner-h.de>  Sun, 12 Jan 2020 13:37:31 +0100

supertuxkart (1.1-1) unstable; urgency=medium

  * Team upload.

  [ Reiner Herrmann ]
  * New upstream release. (Closes: #948222)
    - refresh patches and drop applied ones
  * Link against system mcpp and raqm and drop embedded copies
  * Keep IPv6 disabled, as it would require the embedded copy of enet.
  * Bump Standards-Version to 4.4.1.
  * Update descriptions
    - drop article at beginning
    - don't use first person
  * Update debhelper compat to 12.
    - drop --list-missing from dh_install
  * Update font dependencies/symlinks/copyright.
  * Build-depend on sqlite3.
  * Install upstream manpage instead of outdated packaged one.
  * Exclude a header from orig tarball that is only used in iOS client.
  * Update copyrights.
  * Drop very old version restrictions.
  * Drop autotools-related files from d/clean and d/rules.

  [ QwertyChouskie ]
  * Add Harfbuzz build dependency.
  * Disable openssl, so cmake will continue building with nettle.
  * Cleanup files in d/clean.

 -- Reiner Herrmann <reiner@reiner-h.de>  Tue, 07 Jan 2020 01:18:14 +0100

supertuxkart (1.0-5) unstable; urgency=medium

  * Team upload.
  * Move the atomic check down so that the target is known.

 -- Reiner Herrmann <reiner@reiner-h.de>  Sun, 08 Sep 2019 22:14:48 +0200

supertuxkart (1.0-4) unstable; urgency=medium

  * Team upload.
  * Fix test program that checks for atomic support, by using an
    8 byte type instead of int.

 -- Reiner Herrmann <reiner@reiner-h.de>  Sun, 08 Sep 2019 12:11:32 +0200

supertuxkart (1.0-3) unstable; urgency=medium

  * Team upload.
  * Enable Wayland support.
    - build-depend on libwayland-dev and libxkbcommon-dev
  * Link against libatomic on architectures where it is required.
    (Closes: #934799)

 -- Reiner Herrmann <reiner@reiner-h.de>  Sat, 07 Sep 2019 13:13:25 +0200

supertuxkart (1.0-2) unstable; urgency=medium

  * Team upload.
  * Upload to unstable.
  * Declare compliance with Debian Policy 4.4.0.

 -- Markus Koschany <apo@debian.org>  Sat, 13 Jul 2019 00:51:33 +0200

supertuxkart (1.0-1) experimental; urgency=medium

  * Team upload.

  [ Reiner Herrmann ]
  * New upstream release.
  * Refresh patches and drop obsolete ones.
  * Link against additional system libraries: glew, enet, squish.
  * Update installation path of appdata.
  * Update removed/ignored files during installation.
  * Convert -dbg to automatically generated -dbgsym package.
  * Enable all hardening options.
  * Drop obsolete check for parallel builds.
  * Fix spelling errors found by lintian.
  * Mark -data as Multi-Arch foreign.
  * Declare that d/rules does not require root.
  * Add upstream metadata.
  * Convert d/copyright to copyright format 1.0 and update it.

  [ QwertyChouskie ]
  * Update d/control:
    - Removed SDL deps left over from the pre-0.7 days.
    - Added nettle dependency
    - Added deps for GLES build (1.0 automatically builds with GLES
      on ARM systems)
    - Update description to match supertuxkart.appdata.xml

 -- Reiner Herrmann <reiner@reiner-h.de>  Sun, 09 Jun 2019 22:28:11 +0200

supertuxkart (0.9.3-2) unstable; urgency=medium

  * Team upload.
  * Drop deprecated menu file and debian/supertuxkart.xpm icon.
  * Switch to compat level 11.
  * Declare compliance with Debian Policy 4.3.0.
  * Use canonical VCS URI and move the package to salsa.debian.org.
  * Add glew.patch to fix FTBFS with Debian's latest version of glew.
    Thanks to Adrian Bunk for the report. (Closes: #915453)

 -- Markus Koschany <apo@debian.org>  Sat, 29 Dec 2018 20:30:46 +0100

supertuxkart (0.9.3-1) unstable; urgency=medium

  * New upstream release.
    - Fix segfault in MusicInformation::isPlaying. (Closes: #873670)
    - Fix credits for Boom-boom-boom song. (Closes: #848001)
    - Remove obsolete patches: replace-fonts-ubuntu-with-cantarell.patch,
      update_boom_boom_boom_license.patch, fix_angelscript_ftbfs.patch,
      irrlicht/link-against-needed-libs.diff.
    - Refresh remaining patches.
  * Add new font dependencies for supertuxkart-data: fonts-freefont-ttf,
    fonts-noto-hinted,
  * Bump Standards version to 4.1.2.
    - Change priority of debug package from extra to optional.

 -- Vincent Cheng <vcheng@debian.org>  Sun, 17 Dec 2017 20:16:38 -0800

supertuxkart (0.9.2+dfsg-2) unstable; urgency=medium

  * Fix FTBFS on arm64, mips/mips64/mipsel, ppc64el, s390x. (Closes: #830748)
    - Add debian/patches/fix_angelscript_ftbfs.patch.
  * Remove non-free Ubuntu Font Family fonts, replaced with Cantarell.
    (Closes: #830751)
    - Add new dependency on fonts-cantarell.
    - Add debian/patches/replace-fonts-ubuntu-with-cantarell.patch to replace
      references to Ubuntu Font Family fonts.
  * Clarify licensing for Boom_boom_boom.ogg and add source mod files to
    tarball. (Closes: #832062)
    - Add debian/patches/update_boom_boom_boom_license.patch.

 -- Vincent Cheng <vcheng@debian.org>  Sat, 03 Dec 2016 17:17:52 -0800

supertuxkart (0.9.2-1) unstable; urgency=medium

  * New upstream release.
    - Fix FTBFS with gcc 6. (Closes: #811724)
    - Drop patches fix_angelscript_build_on_non-x86_arches.patch and
      srgb_workaround.patch; applied upstream.
    - Refresh remaining patches.
  * Add build-depends on libfreetype6-dev.
  * Update Homepage field in d/control.
  * Bump Standards version to 3.9.8, no changes required.

 -- Vincent Cheng <vcheng@debian.org>  Thu, 07 Jul 2016 23:40:10 -0700

supertuxkart (0.9.1-3) unstable; urgency=medium

  * Add srgb_workaround.patch to provide workaround for darker screens on
    Intel GPUs with mesa >= 10.6. (Closes: #808857) More info upstream at:
    https://github.com/supertuxkart/stk-code/issues/2190

 -- Vincent Cheng <vcheng@debian.org>  Wed, 23 Dec 2015 13:07:40 -0800

supertuxkart (0.9.1-2) unstable; urgency=medium

  * Fix fix_angelscript_build_on_non-x86_arches.patch to fix FTBFS on armhf.
    (Closes: #808716)

 -- Vincent Cheng <vcheng@debian.org>  Tue, 22 Dec 2015 21:29:36 -0800

supertuxkart (0.9.1-1) unstable; urgency=medium

  * New upstream release. (Closes: #808355)
    - Drop support_windowed_mode_when_xrandr_not_available.patch, applied
      upstream; refresh remaining patches.
  * Remove debian/menu file as per tech-ctte decision in #741573.

 -- Vincent Cheng <vcheng@debian.org>  Mon, 21 Dec 2015 13:51:09 -0800

supertuxkart (0.9-5) unstable; urgency=medium

  * Add patch to fix crash when xrandr is not available. (Closes: #793457)

 -- Vincent Cheng <vcheng@debian.org>  Sat, 25 Jul 2015 17:57:15 -0700

supertuxkart (0.9-4) unstable; urgency=medium

  * Update fix_angelscript_build_on_non-x86_arches.patch again to fix FTBFS
    on armel.

 -- Vincent Cheng <vcheng@debian.org>  Wed, 03 Jun 2015 20:26:12 -0700

supertuxkart (0.9-3) unstable; urgency=medium

  * Update fix_angelscript_build_on_non-x86_arches.patch to fix FTBFS on
    armel. Thanks again to James Cowgill for the patch!

 -- Vincent Cheng <vcheng@debian.org>  Mon, 01 Jun 2015 21:39:01 -0700

supertuxkart (0.9-2) unstable; urgency=medium

  * Add fix_angelscript_build_on_non-x86_arches.patch to fix FTBFS on a few
    non-x86 arches (arm*, ppc64el, s390x). Thanks to James Cowgill for the
    patch!
  * Install upstream appdata. Thanks to Marcus Lundblad for the patch!
    (Closes: #785516)
  * Drop build-depends on libglew-dev; add lintian override for embedded glew.

 -- Vincent Cheng <vcheng@debian.org>  Sun, 17 May 2015 21:50:45 -0700

supertuxkart (0.9-1) unstable; urgency=medium

  * Team upload.

  [ Vincent Cheng ]
  * New upstream release.
    - Drop fix_desktop_file.patch, applied upstream.
    - Rename build_against_system_enet.patch to
      build_against_system_enet_and_glew.patch, and build against system glew.
    - Refresh remaining patches.
  * Add build-depends on libxrandr-dev, libglew-dev.
  * Update my email address.
  * Bump Standards version to 3.9.6, no changes required.

  [ Christoph Egger ]
  * remove myself from uploaders

 -- Vincent Cheng <vcheng@debian.org>  Wed, 13 May 2015 00:32:20 -0700

supertuxkart (0.8.1-2) unstable; urgency=medium

  * Disable support for wiimote input devices on !linux (since
    libbluetooth-dev is only built on linux archs).
  * Restrict build-dep libbluetooth-dev to linux-any.

 -- Vincent Cheng <Vincentc1208@gmail.com>  Fri, 20 Dec 2013 14:58:35 -0800

supertuxkart (0.8.1-1) unstable; urgency=low

  * Team upload

  [ Alexander Reichle-Schmehl ]
  * Removed myself from uploaders.

  [ Gianfranco Costamagna ]
  * New upstream release, patch refresh
  * Bumped std-version to 3.9.5, no changes required
  * Added libbluetooth-dev
  * Added lib/irrlicht/lib/Linux/libIrrlicht.a
    to debian/clean target
  * Moved images from xpm to png, since upstream has changed them

  [ Vincent Cheng ]
  * Run wrap-and-sort -sa
  * Re-added xpm icon for debian/menu.
  * Fix lintian error embedded-library (libjpeg, libpng, zlib).

 -- Vincent Cheng <Vincentc1208@gmail.com>  Thu, 19 Dec 2013 23:18:54 -0800

supertuxkart (0.8-2) unstable; urgency=low

  * Upload to unstable.
  * Add override for outdated-autotools-helper-file (cmake is used instead).
  * Fix lintian warning vcs-field-not-canonical.

 -- Vincent Cheng <Vincentc1208@gmail.com>  Sun, 05 May 2013 19:26:37 -0700

supertuxkart (0.8-1) experimental; urgency=low

  * New upstream release.
    - Use embedded/forked copy of irrlicht; refer to debian/README.source for
      details. Also import patches used in Debian's irrlicht source package.
    - Remove build-depends on libirrlicht-dev.
    - Add build-depends on zlib1g-dev, libbz2-dev, libpng-dev, libjpeg-dev,
      mesa-common-dev.
    - Remove backport_cmake.patch and build_with_irrlicht_1.8.patch; applied
      upstream.
    - Refresh remaining patches.
  * Bump Standards version to 3.9.4, no changes required.

 -- Vincent Cheng <Vincentc1208@gmail.com>  Fri, 28 Dec 2012 22:45:14 -0800

supertuxkart (0.7.3-2+exp1) experimental; urgency=low

  * Upload to experimental.
  * Build with irrlicht 1.8.
    - This should fix segfaults related to irrlicht 1.7.3 in supertuxkart,
      commonly triggered through the use of power-ups during races.
      (Closes: #677609; LP: #1011180, LP: #1048284, LP: #1049398, LP: #1061436,
      LP: #1064019, LP: #1069871)
    - This also fixes incorrect rendering of Suzanne's kart. (Closes: #679837)
    - Update build-depends on libirrlicht-dev (>= 1.8).
    - Drop patch debian/patches/build_with_irrlicht_1.7.3.patch and add
      debian/patches/build_with_irrlicht_1.8.patch.
  * Add new package supertuxkart-dbg containing debugging symbols.

 -- Vincent Cheng <Vincentc1208@gmail.com>  Fri, 16 Nov 2012 08:22:15 -0800

supertuxkart (0.7.3-2) unstable; urgency=low

  * Switch buildsystem to cmake (upstream has deprecated their autotools
    build system).
    - Drop build-depends on autotools-dev, automake, dh-autoreconf.
    - Add build-depends on cmake (>= 2.8.1).
    - Refresh debian/patches/build_against_system_enet.patch.
    - Add debian/patches/backport_cmake.patch to backport fixes to STK's
      CMake build system (and to make it easier to build against system enet).
  * Install hi-res icon and use it in STK's desktop menu entry. (LP: #937976)
  * Enable parallel building.

 -- Vincent Cheng <Vincentc1208@gmail.com>  Thu, 31 May 2012 17:23:37 -0700

supertuxkart (0.7.3-1) unstable; urgency=low

  * Team upload.

  [ Alexander Reichle-Schmehl ]
  * New upstream release (Closes: #634019, LP: #871630)
  * Fix watchfile to ignore release candidates
  * Refresh patches/build-against-system-enet.patch
  * Drop patches/enet1.3.patch (Applied upstream)

  [ Vincent Cheng ]
  * Add myself to Uploaders.
  * Add debian/patches/build_with_irrlicht_1.7.3.patch to enable build with
    latest stable version of irrlicht rather than an unreleased SVN revision.
  * Bump versioned dependency on libirrlicht-dev to (>= 1.7.3).
  * Update debian/rules to dh 7 "rules.tiny" style.
  * Add build-depends on dh-autoreconf and use autoreconf sequence in
    debian/rules.
  * Add build-depends on libcurl4-gnutls-dev | libcurl4-dev, libfribidi-dev,
    libxxf86vm-dev.
  * Add missing DEP-3 headers to patches.
  * Source tarball is now DFSG-clean.
    - Remove unneeded get-orig-source target in debian/rules.
    - Remove unneeded get-orig-source.sh and README.source.
  * Remove unneeded *.xpm icons in debian/.
  * Remove unused lintian-overrides file.
  * Remove unneeded debian/supertuxkart.desktop menu file (use menu file in
    data/supertuxkart_desktop.template instead).
    - Add debian/patches/fix_desktop_file.patch to add Debian/Ubuntu-specific
      changes to menu file (and forward patch upstream). (Closes: #667640;
      LP: #709987)
  * "Modernize" package description, as suggested by an upstream dev.
    (LP: #774345); remove reference to GotM. (Closes: #531637)
  * Bump dh compat level from 7 to 9.
  * Bump standards version from 3.8.3 to 3.9.3 (no changes needed).

 -- Vincent Cheng <Vincentc1208@gmail.com>  Fri, 04 May 2012 18:36:07 -0700

supertuxkart (0.7+dfsg1-2) unstable; urgency=low

  * drop 3DGraphics from desktop file (LP: #532065)
  * Apply patch by Ansgar Burchardt to build against enet 1.3 (Closes:
    #617783)

 -- Christoph Egger <christoph@debian.org>  Thu, 17 Mar 2011 21:08:17 +0100

supertuxkart (0.7+dfsg1-1) unstable; urgency=low

  * New upstream release

 -- Christoph Egger <christoph@debian.org>  Thu, 24 Feb 2011 22:36:25 +0100

supertuxkart (0.6.2+dfsg1-2) unstable; urgency=low

  * Change my E-Mail Address
  * Kill some whitespaces at the end of lines
  * Explicitly link against libGLU (Closes: #556477) taking the patch from
    Ubuntu
  * Remove KiBi from Uploaders on his request
  * Build against system libenet, makes stk build on GNU/kFreeBSD and is
    the right thing anyway
  * Add misc:Depends to -data package
  * Bump standards version from 3.8.3 to .4 (no changes needed)

 -- Christoph Egger <christoph@debian.org>  Wed, 10 Mar 2010 23:51:14 +0100

supertuxkart (0.6.2+dfsg1-1) unstable; urgency=low

  * New Upstream Release
    * Only Bugfixes and minor improvements
    * Drop rubber.patch again as it was taken from 0.6.2 branch
      and is part of this release
    * Use replacement artwork from 0.6.1a
  * Silence horn.wav (Closes: #515332)
  * Bump Standards Version to 3.8.3 (No changes needed)
  * Bump debhelper compat to 7 for dh_prep
  * Update our copy of supertuxkart.desktop from upstreams,
    our changes will get pushed upstream as well.
  * Cleaning debian/rules a bit more

 -- Christoph Egger <debian@christoph-egger.org>  Sat, 22 Aug 2009 12:45:50 +0200

supertuxkart (0.6.1a+dfsg2-3) unstable; urgency=low

  * Move config.* out of the diff (Closes: #538617)
  * Clean up rules
  * lintian override for invalid spellchecker in binary
  * Bump standards Version without any changes
  * Reorder Categories in the desktop file (Closes: #443412) (LP: #329265)
  * Add rubber.patch so supertuxkart won't segfault in RubberBand::hit anymore.
    (Closes: #539964)

 -- Christoph Egger <debian@christoph-egger.org>  Mon, 03 Aug 2009 22:26:02 +0200

supertuxkart (0.6.1a+dfsg2-2) unstable; urgency=low

  [ Alexander Reichle-Schmehl ]
  * Adapt debian/control to my new name

  [ Cyril Brulebois ]
  * Update my mail address

  [ Christoph Egger ]
  * Fix get-orig-source rule
  * Fix watchfile
  * Unversion depends on plib (Closes: #516925)

 -- Christoph Egger <debian@christoph-egger.org>  Tue, 24 Feb 2009 15:31:32 +0100

supertuxkart (0.6.1a+dfsg2-1) unstable; urgency=low

  * Now use the correct tarball (was the old 0.5 by accident)

 -- Christoph Egger <debian@christoph-egger.org>  Sun, 22 Feb 2009 13:32:47 +0100

supertuxkart (0.6.1a+dfsg1-1) unstable; urgency=low

  [ Peter De Wachter ]
  * Added patch to check for SDL errors and to allow the game to run on
    low-end hardware (closes: #501116), (LP: #203144); Applied Upstream

  [ Christoph Egger ]
  * New Upstream Release (Closes: #512994), (LP: #319990)
    * Fix incorrect recreation of items
    * Correct Issue when restarting follow-the-leader game
    * Fix build with GCC 4.4 (Closes: #505671)
    * Drop all patches (now upstream)
  * Use $(QUILT_STAMPFN) insted of direct patch target for quilt make-snippet
  * Wrap Uploaders, Build-Depends
  * get-orig-source target
  * mangle watchfile correctly

  [ Stefan Potyra ]
  * debian/supertuxcart.sgml: fix dhusername entity.

 -- Christoph Egger <debian@christoph-egger.org>  Wed, 18 Feb 2009 18:13:28 +0100

supertuxkart (0.5+dfsg1-1) unstable; urgency=medium

  * urgency medium due to RC fix
  * Replacing nonfree stuff (Closes: #514416)
  * Improving debian/copyright
  * Update my Email address

 -- Christoph Egger <debian@christoph-egger.org>  Sun, 08 Feb 2009 19:38:27 +0100

supertuxkart (0.5-1.1) unstable; urgency=high

  * Non-maintainer upload.
  * DEB_BUILD_OPTIONS=noopt debian/rules now sets CXXFLAGS (closes: #490045)
  * Tighten up build-dep on plib1.8.4-dev (closes: #490052 as a side-effect)
  * urgency=high to fix RC bug in testing

 -- Vincent Fourmond <fourmond@debian.org>  Mon, 21 Jul 2008 00:34:36 +0200

supertuxkart (0.5-1) unstable; urgency=low

  * New upstream release (Closes: #484326)
    * Upstream no longer ships .desktop file
      So no need to remove it in rules
  * Bumping Standards Version to 3.8.0
    * Adding debian/README.source
  * Adding myself to Uploaders
  * Fixing multiplayer bug (First player gets
    playerkart with best ranking after first map
    in grandprix
  * bug1996464.patch (from upstream bugtracker)

 -- Christoph Egger <Christoph.Egger@gmx.de>  Thu, 19 Jun 2008 18:58:49 +0200

supertuxkart (0.4-1) unstable; urgency=low

  * New upstream release (Closes: #470552).
  * 10_fix_FTBFS_with_gcc-4.3.diff:
    + Patch for gcc-4.3 FTBFS is no longer needed (Closes: #455187).

 -- Sam Hocevar (Debian packages) <sam+deb@zoy.org>  Tue, 11 Mar 2008 22:52:53 +0000

supertuxkart (0.3-3) UNRELEASED; urgency=low

  * Introduce quilt patch system.
  * Fix FTBFS with gcc-4.3 (missing headers) by adding the following
    patch (Closes: #455187):
     + 10_fix_FTBFS_with_gcc-4.3.diff.
  * Remove obsolete “Encoding” key from debian/supertuxkart.desktop.
  * Add myself to the Uploaders.
  * Add two tiny items to debian/TODO.

 -- Cyril Brulebois <cyril.brulebois@enst-bretagne.fr>  Fri, 29 Feb 2008 01:03:20 +0100

supertuxkart (0.3-2) unstable; urgency=low

  [ Barry deFreese ]
  * Add watch file
  * Add Homepage field to control
  * Remove XS- from VCS fields

  [ Gonéri Le Bouder ]
  * fix spelling error in the description, thanks Philippe Cloutier
    and  Filipus Klutier (Closes: #441419)

  [ Jon Dowland ]
  * add Homepage: control field to source stanza

  [ Alexander Schmehl ]
  * adding libvorbis-dev to build-depends, appearantly supertuxkart supports
    ogg vorbis; thanks to Lucas Nussbaum for detecting via build daemon from
    hell
  * removing Homepage: semie-headers from package descriptions
  * reformating long description to not be longer than 80 columns
  * debian/copyright refering to /usr/share/common-licenses/GPL-2 instead of
    GPL
  * Bumping standards version to 3.7.3: No changes needed

 -- Alexander Schmehl <tolimar@debian.org>  Tue, 29 Jan 2008 22:15:13 +0100

supertuxkart (0.3-1) unstable; urgency=low

  [ Gonéri Le Bouder ]
  * New upstream release
   - updated copyright file
   - new b-deps: libopenal-dev, libalut-dev, libmikmod2-dev
  * install the desktop file (Closes: 406873)
  * do not ignore the make distclean return anymore
  * versionned dependency against plib >= 1.8.4-8 because of #436917

  [ Sam Hocevar ]
  * debian/control:
    + Remove leading “a”s from short descriptions.
    + Use ${source:Version} to make package binNMUable.
    + Added XS-Vcs-Svn field.

  [ Jon Dowland ]
  * update menu section to "Games/Action" for menu policy transition.
    Thanks Linas Žvirblis.

  [ Cyril Brulebois ]
  * Added XS-Vcs-Browser fields in the control file.

 -- Gonéri Le Bouder <goneri@rulezlan.org>  Thu,  9 Aug 2007 12:07:57 +0200

supertuxkart (0.2-1) unstable; urgency=low

  [ Gonéri Le Bouder ]
  * new upstream release candidate
   + Closes: #388021
   + remove supertuxkart.sh
   + add supertuxkart(|-data).install
   + clean up
   + remove deps on ${shlibs:Depends}, ${misc:Depends} for supertuxkart-data
  * fix French comment was tagged de_DE
  * fix not-binnmuable-any-depends-all
  * fix FTBFS on 64bit arch
   + Closes: #370810

  [ Eddy Petrişor ]
  * added Romanian translation to desktop file

 -- Gonéri Le Bouder <goneri@rulezlan.org>  Fri,  8 Sep 2006 22:59:25 +0200

supertuxkart (0.0.0.1-2) unstable; urgency=low

  * supertuxkart-data architecture fixed to all
  * fix circular dependency
  * better description for supertuxkart-data
    Closes: #370673

 -- Gonéri Le Bouder <goneri@rulezlan.org>  Tue,  6 Jun 2006 13:09:38 +0200

supertuxkart (0.0.0.1-1) unstable; urgency=low

  * Initial release Closes: #366199

 -- Gonéri Le Bouder <goneri@rulezlan.org>  Sat,  6 May 2006 02:15:26 +0000
